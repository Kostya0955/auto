class CreateCars < ActiveRecord::Migration[5.1]
  def change
    create_table :cars do |t|
      t.string :model
      t.string :brand
      t.string :number
      t.integer :year
      t.integer :mileage

      t.timestamps
    end
  end
end
